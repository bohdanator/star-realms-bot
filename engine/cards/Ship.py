from cards.Card import Card
from game.constants import CardColor, CardType

class Ship(Card):
    def __init__(self, name: str, colors: list[CardColor], cost: int, scrapable: bool =False, alliable: bool =True):
        super().__init__(name, colors, cost, scrapable, alliable)
        self.type = CardType.SHIP
