from game.constants import CardColor, CardType
from game.GameState import GameState
from cards.Base import Base
from cards.Ship import Ship
from game.gamestate_functions import *
from game.Command import Command

# BASIC CARDS

class Scout(Ship):
    def __init__(self):
        super().__init__('Scout', [CardColor.NONE], 0, False, False)

    def primary_action(self, gs: GameState):
        gs.add_trade(1)

class Viper(Ship):
    def __init__(self):
        super().__init__('Viper', [CardColor.NONE], 0, False, False)

    def primary_action(self, gs: GameState):
        gs.add_combat(1)

class Explorer(Ship):
    def __init__(self):
        super().__init__('Explorer', [CardColor.NONE], 2, True, False)

    def primary_action(self, gs: GameState):
        gs.add_trade(2)

    def scrap_action(self, gs: GameState):
        gs.add_combat(2)

### CORE SET ###
### BLOB ###

class BattleBlob(Ship):
    def __init__(self):
        super().__init__('Battle Blob', [CardColor.GREEN], 6, True)

    def primary_action(self, gs: GameState):
        gs.add_combat(8)

    def secondary_action(self, gs: GameState):
        gs.draw()

    def scrap_action(self, gs: GameState):
        gs.add_combat(4)

class BattlePod(Ship):
    def __init__(self):
        super().__init__('Battle Pod', [CardColor.GREEN], 2, False)

    def primary_action(self, gs: GameState):
        gs.add_combat(4)
        gs.player_on_turn.command_queue.append(
            create_trade_row_scrap_queue(gs)
        )

    def secondary_action(self, gs: GameState):
        gs.add_combat(2)

class BlobCarrier(Ship):
    def __init__(self):
        super().__init__('Blob Carrier', [CardColor.GREEN], 6, False)

    def primary_action(self, gs: GameState):
        gs.add_combat(7)

    def secondary_action(self, gs: GameState):
        gs.player_on_turn.command_queue.append(
            create_free_ship_on_topdeck_queue(gs)
        )

class BlobDestroyer(Ship):
    def __init__(self):
        super().__init__('Blob Destroyer', [CardColor.GREEN], 4, False)

    def primary_action(self, gs: GameState):
        gs.add_combat(6)

    def secondary_action(self, gs: GameState):
        gs.command_queue.append(
            create_destroy_base_queue(gs) + [do_nothing_command]
        )
        gs.command_queue.append(
            create_trade_row_scrap_queue(gs) + [do_nothing_command]
        )

class BlobFighter(Ship):
    def __init__(self):
        super().__init__('Blob Fighter', [CardColor.GREEN], 1, False)

    def primary_action(self, gs: GameState):
        gs.add_combat(3)

    def secondary_action(self, gs: GameState):
        gs.draw()

class BlobWheel(Base):
    def __init__(self):
        super().__init__('Blob Wheel', [CardColor.GREEN], 3, defence=5, outpost=False, scrapable=True)

    def primary_action(self, gs: GameState):
        gs.add_combat(1)

    def scrap_action(self, gs: GameState):
        gs.add_trade(3)

class BlobWorld(Base):
    def __init__(self):
        super().__init__('Blob World', [CardColor.GREEN], 8, defence=7, outpost=False, alliable=False)

    def primary_action(self, gs: GameState):
        def actionA(gss):
            gss.add_combat(5)

        def actionB(gss):
            for _ in range(gss.colors_played[CardColor.GREEN]):
                gss.draw()
        gs.command_queue.append([
            Command(
                actionA,
                'CHOICE 1|add 5 combat|BlobWorld',
            ),
            Command(
                actionB,
                'CHOICE 2|draw a card for each Blob card you\'ve played this turn|Blob World'
            )
        ])

class Mothership(Ship):
    def __init__(self):
        super().__init__('Mothership', [CardColor.GREEN], 7, False)

    def primary_action(self, gs: GameState):
        gs.add_combat(6)
        gs.draw()

    def secondary_action(self, gs: GameState):
        gs.draw()

class Ram(Ship):
    def __init__(self):
        super().__init__('Ram', [CardColor.GREEN], 3, scrapable=True)

    def primary_action(self, gs: GameState):
        gs.add_combat(5)

    def secondary_action(self, gs: GameState):
        gs.add_combat(2)

    def scrap_action(self, gs: GameState):
        gs.add_trade(3)

class TheHive(Base):
    def __init__(self):
        super().__init__('The Hive', [CardColor.GREEN], 5, defence=5, outpost=False, scrapable=False)

    def primary_action(self, gs: GameState):
        gs.add_combat(3)

    def secondary_action(self, gs: GameState):
        gs.draw()

class TradePod(Ship):
    def __init__(self):
        super().__init__('Trade Pod', [CardColor.GREEN], 2)

    def primary_action(self, gs: GameState):
        gs.add_trade(3)

    def secondary_action(self, gs: GameState):
        gs.add_combat(2)

### MACHINE CULT ###

class BattleMech(Ship):
    def __init__(self):
        super().__init__('Battle Mech', [CardColor.RED], 5)

    def primary_action(self, gs: GameState):
        gs.add_combat(4)
        add_serial(gs, create_paralell(
            gs,
            create_hand_scrap_queue,
            create_discard_scrap_queue,
            do_nothing_queue,
        ))(gs)

    def secondary_action(self, gs: GameState):
        gs.add_combat(2)

class BattleStation(Base):
    def __init__(self):
        super().__init__('Battle Station', [CardColor.RED], 5, defence=5, outpost=True, scrapable=True, alliable=False)

    def scrap_action(self, gs: GameState):
        gs.add_combat(5)

class BrainWorld(Base):
    def __init__(self):
        super().__init__('Brain World', [CardColor.RED], 8, defence=6, outpost=True)

    def primary_action(self, gs: GameState):
        sc = gs.cards_scrapped
        def draw_function(gs):
            for _ in range(gs.cards_scrapped - draw_function.sc):
                gs.draw()
        draw_function.sc = sc
        add_serial(
            gs,
            create_paralell(
                gs,
                create_hand_scrap_queue,
                create_discard_scrap_queue,
                do_nothing_queue,
            ),
            add_serial(
                gs,
                create_paralell(
                    gs,
                    create_hand_scrap_queue,
                    create_discard_scrap_queue,
                    do_nothing_queue,
                ),
                draw_function
            )
        )(gs)

class Junkyard(Base):
    def __init__(self):
        super().__init__('Junkyard', [CardColor.RED], 6, defence=5, outpost=True, alliable=False)

    def primary_action(self, gs: GameState):
        add_serial(gs, create_paralell(
            gs,
            create_hand_scrap_queue,
            create_discard_scrap_queue,
        ))(gs)

class MachineBase(Base):
    def __init__(self):
        super().__init__('Machine Base', [CardColor.RED], 7, defence=6, outpost=True, alliable=False)

    def primary_action(self, gs: GameState):
        gs.draw()
        add_serial(gs, create_hand_scrap_queue)(gs)

class MechWorld(Base):
    def __init__(self):
        super().__init__('Mech World', [CardColor.RED, CardColor.GREEN, CardColor.BLUE, CardColor.YELLOW], 5, defence=6, outpost=True, alliable=False)

class MissileBot(Ship):
    def __init__(self):
        super().__init__('Missile Bot', [CardColor.RED], 2)

    def primary_action(self, gs: GameState):
        gs.add_combat(2)
        add_serial(gs, create_paralell(
            gs,
            create_hand_scrap_queue,
            create_discard_scrap_queue,
            do_nothing_queue,
        ))(gs)

    def secondary_action(self, gs: GameState):
        gs.add_combat(2)

class MissileMech(Ship):
    def __init__(self):
        super().__init__('Missile Mech', [CardColor.RED], 6)

    def primary_action(self, gs: GameState):
        gs.add_combat(6)
        add_serial(gs, create_paralell(
            gs,
            create_destroy_base_queue,
            do_nothing_queue,
        ))(gs)

    def secondary_action(self, gs: GameState):
        gs.draw()

class PatrolMech(Ship):
    def __init__(self):
        super().__init__('Patrol Mech', [CardColor.RED], 4)

    def primary_action(self, gs: GameState):
        gs.command_queue.append([
            Command(lambda g: g.add_trade(3), 'CHOICE 1|Add 3 Trade|Patrol Mech'),
            Command(lambda g: g.add_combat(5), 'CHOICE 2|Add 5 Combat|Patrol Mech'),
        ])

    def secondary_action(self, gs: GameState):
        add_serial(gs, create_paralell(
            gs,
            create_hand_scrap_queue,
            create_discard_scrap_queue,
            do_nothing_queue,
        ))(gs)

class StealthNeedle(Ship):
    def __init__(self):
        super().__init__('Stealth Needle', [CardColor.RED], 4)
        self.target = None

    def primary_action(self, gs: GameState):
        def wrapper(gs, stealth_needle, target):
            stealth_needle.target = target
            for c in target.colors:
                if c != CardColor.RED:
                    stealth_needle.colors.append(c)
            stealth_needle.scrapable = target.scrapable
            stealth_needle.name = f'Stealth Needle - Copy of {target.name}'
            target.primary_action(gs)

        commands = []
        for c in gs.player_on_turn.battlefield:
            if c.type == CardType.SHIP and c.name != 'Stealth Needle':
                commands.append(Command(
                    wrapper,
                    f'CHOICE {c.name}|Choose card f{c.name} for Stealth Needle to copy|Stealth Needle',
                    self,
                    c,
                ))
        gs.command_queue.append(commands)

    def secondary_action(self, gs: GameState):
        if self.target is not None: self.target.secondary_action(gs)

    def scrap_action(self, gs: GameState):
        if self.target is not None: self.target.scrap_action(gs)

    def reset(self):
        super().reset()
        self.target = None
        self.scrapable = False
        self.name = 'Stealth Needle'
        self.colors = [CardColor.RED]

class SupplyBot(Ship):
    def __init__(self):
        super().__init__('Supply Bot', [CardColor.RED], 3)

    def primary_action(self, gs: GameState):
        gs.add_trade(2)
        add_serial(gs, create_paralell(
            gs,
            create_hand_scrap_queue,
            create_discard_scrap_queue,
            do_nothing_queue,
        ))(gs)

    def secondary_action(self, gs: GameState):
        gs.add_combat(2)

class TradeBot(Ship):
    def __init__(self):
        super().__init__('Trade Bot', [CardColor.RED], 1)

    def primary_action(self, gs: GameState):
        gs.add_trade(1)
        add_serial(gs, create_paralell(
            gs,
            create_hand_scrap_queue,
            create_discard_scrap_queue,
            do_nothing_queue,
        ))(gs)

    def secondary_action(self, gs: GameState):
        gs.add_combat(2)

### STAR EMPIRE ###

class Battlecruiser(Ship):
    def __init__(self):
        super().__init__('Battlecruiser', [CardColor.YELLOW], 6, scrapable=True)

    def primary_action(self, gs: GameState):
        gs.add_combat(5)
        gs.draw()

    def secondary_action(self, gs: GameState):
        add_serial(gs, create_player_discard_queue)(gs)

    def scrap_action(self, gs: GameState):
        gs.draw()
        add_serial(gs, create_paralell(
            gs,
            create_destroy_base_queue,
            do_nothing_queue,
        ))(gs)

class Corvette(Ship):
    def __init__(self):
        super().__init__('Corvette', [CardColor.YELLOW], 2)

    def primary_action(self, gs: GameState):
        gs.add_combat(1)
        gs.draw()

    def secondary_action(self, gs: GameState):
        gs.add_combat(2)

class Dreadnaught(Ship):
    def __init__(self):
        super().__init__('Dreadnaught', [CardColor.YELLOW], 7, scrapable=True, alliable=False)

    def primary_action(self, gs: GameState):
        gs.add_combat(7)
        gs.draw()

    def scrap_action(self, gs: GameState):
        gs.add_combat(5)

class FleetHQ(Base):
    def __init__(self):
        super().__init__('Fleet HQ', [CardColor.YELLOW], 8, 8, outpost=False, alliable=False)

    def primary_action(self, gs: GameState):
        gs.play_triggers.append(lambda gs, card: gs.add_combat(1) if card.type == CardType.SHIP else None)

class ImperialFighter(Ship):
    def __init__(self):
        super().__init__('Imperial Fighter', [CardColor.YELLOW], 1)

    def primary_action(self, gs: GameState):
        gs.add_combat(2)
        add_serial(gs, create_player_discard_queue)(gs)

    def secondary_action(self, gs: GameState):
        gs.add_combat(2)

class ImperialFrigate(Ship):
    def __init__(self):
        super().__init__('Imperial Frigate', [CardColor.YELLOW], 3, scrapable=True)

    def primary_action(self, gs: GameState):
        gs.add_combat(4)
        add_serial(gs, create_player_discard_queue)(gs)

    def secondary_action(self, gs: GameState):
        gs.add_combat(2)

    def scrap_action(self, gs: GameState):
        gs.draw()

class RecyclingStation(Base):
    def __init__(self):
        super().__init__('Recycling Station', [CardColor.YELLOW], 4, 4, outpost=True, alliable=False)

    def primary_action(self, gs: GameState):
        def discard1(gs):
            add_serial(
                gs,
                create_discard_queue,
                lambda gs: gs.draw(),
            )(gs)
        def discard2(gs):
            add_serial(
                gs,
                create_discard_queue,
                add_serial(
                    gs,
                    create_discard_queue,
                    lambda g: g.draw(2),
                )
            )(gs)
        commands = [
            Command(lambda g: g.add_trade(1), 'CHOICE 1|Add 1 trade|Recycling Station'),
            Command(dn, 'CHOICE 2|Discard 0 cards, then draw 0 cards|Recycling Station'),
            Command(discard1, 'CHOICE 3|Discard 1 card, then draw 1 card|Recycling Station'),
            Command(discard2, 'CHOICE 4|Discard 2 cards, then draw 2 cards|Recycling Station')
        ]
        gs.command_queue.append(commands)

class RoyalRedoubt(Base):
    def __init__(self):
        super().__init__('Royal Redoubt', [CardColor.YELLOW], 6, 6, outpost=True)

    def primary_action(self, gs: GameState):
        gs.add_combat(3)

    def secondary_action(self, gs: GameState):
        add_serial(gs, create_player_discard_queue)(gs)

class SpaceStation(Base):
    def __init__(self):
        super().__init__('Space Station', [CardColor.YELLOW], 4, 4, outpost=True, scrapable=True, alliable=True)

    def primary_action(self, gs: GameState):
        gs.add_combat(2)

    def secondary_action(self, gs: GameState):
        gs.add_combat(2)

    def scrap_action(self, gs: GameState):
        gs.add_trade(4)

class SurveyShip(Ship):
    def __init__(self):
        super().__init__('Survey Ship', [CardColor.YELLOW], 3, scrapable=True, alliable=False)

    def primary_action(self, gs: GameState):
        gs.add_trade(1)
        gs.draw()

    def scrap_action(self, gs: GameState):
        add_serial(gs, create_player_discard_queue)(gs)

class WarWorld(Base):
    def __init__(self):
        super().__init__('War World', [CardColor.YELLOW], 5, 4, outpost=True)

    def primary_action(self, gs: GameState):
        gs.add_combat(3)

    def secondary_action(self, gs: GameState):
        gs.add_combat(4)

### TRADE FEDERATION ###

class BarterWorld(Base):
    def __init__(self):
        super().__init__('Barter World', [CardColor.BLUE], 5, 4, outpost=False, scrapable=True, alliable=False)

    def primary_action(self, gs: GameState):
        commands = [
            Command(lambda g: g.add_authority(2), 'CHOICE 1|Add 2 Authority|Barter World'),
            Command(lambda g: g.add_trade(2), 'CHOICE 2|Add 2 Trade|Barter World'),
        ]
        gs.command_queue.append(commands)

    def scrap_action(self, gs: GameState):
        gs.add_combat(5)

class CentralOffice(Base):
    def __init__(self):
        super().__init__('Central Office', [CardColor.BLUE], 7, 6, outpost=False)

    def primary_action(self, gs: GameState):
        gs.add_trade(2)
        gs.next_on_topdeck = True

    def secondary_action(self, gs: GameState):
        gs.draw()

class CommandShip(Ship):
    def __init__(self):
        super().__init__('Command Ship', [CardColor.BLUE], 8)

    def primary_action(self, gs: GameState):
        gs.add_combat(5)
        gs.add_authority(4)
        gs.draw(2)

    def secondary_action(self, gs: GameState):
        add_serial(gs, create_paralell(
            gs,
            create_destroy_base_queue,
            do_nothing_queue,
        ))(gs)

class Cutter(Ship):
    def __init__(self):
        super().__init__('Cutter', [CardColor.BLUE], 2)

    def primary_action(self, gs: GameState):
        gs.add_trade(2)
        gs.add_authority(4)

    def secondary_action(self, gs: GameState):
        gs.add_combat(4)

class DefenseCenter(Base):
    def __init__(self):
        super().__init__('Defense Center', [CardColor.BLUE], 5, 5, outpost=True, alliable=False)

    def primary_action(self, gs: GameState):
        commands = [
            Command(lambda g: g.add_authority(3), 'CHOICE 1|Add 3 Authority|Defence Center'),
            Command(lambda g: g.add_combat(2), 'CHOICE 2|Add 2 Combat|Defence Center'),
        ]
        gs.command_queue.append(commands)

    def scrap_action(self, gs: GameState):
        gs.add_combat(2)

class EmbassyYacht(Ship):
    def __init__(self):
        super().__init__('Embassy Yacht', [CardColor.BLUE], 3, alliable=False)

    def primary_action(self, gs: GameState):
        gs.add_trade(2)
        gs.add_authority(3)
        b = 0
        for c in gs.player_on_turn.battlefield:
            if c.type == CardType.BASE:
                b += 1
        if b >= 2:
            gs.draw(2)

class FederationShuttle(Ship):
    def __init__(self):
        super().__init__('Federation Shuttle', [CardColor.BLUE], 1)

    def primary_action(self, gs: GameState):
        gs.add_trade(2)

    def secondary_action(self, gs: GameState):
        gs.add_authority(4)

class Flagship(Ship):
    def __init__(self):
        super().__init__('Flagship', [CardColor.BLUE], 6)

    def primary_action(self, gs: GameState):
        gs.add_combat(5)
        gs.draw()

    def secondary_action(self, gs: GameState):
        gs.add_authority(5)

class Freighter(Ship):
    def __init__(self):
        super().__init__('Freighter', [CardColor.BLUE], 4)

    def primary_action(self, gs: GameState):
        gs.add_trade(4)

    def secondary_action(self, gs: GameState):
        gs.next_on_topdeck = True

class PortofCall(Base):
    def __init__(self):
        super().__init__('Port of Call', [CardColor.BLUE], 6, 6, outpost=True, scrapable=True, alliable=False)

    def primary_action(self, gs: GameState):
        gs.add_trade(3)

    def scrap_action(self, gs: GameState):
        gs.draw()
        add_serial(gs, create_paralell(
            gs,
            create_destroy_base_queue,
            do_nothing_queue,
        ))(gs)

class TradeEscort(Ship):
    def __init__(self):
        super().__init__('Trade Escort', [CardColor.BLUE], 5)

    def primary_action(self, gs: GameState):
        gs.add_authority(4)
        gs.add_combat(4)

    def secondary_action(self, gs: GameState):
        gs.draw()

class TradingPost(Base):
    def __init__(self):
        super().__init__('Trading Post', [CardColor.BLUE], 3, 4, outpost=True, scrapable=True, alliable=False)

    def primary_action(self, gs: GameState):
        commands = [
            Command(lambda g: g.add_authority(1), 'CHOICE 1|Add 1 Authority|Trading Post'),
            Command(lambda g: g.add_trade(1), 'CHOICE 2|Add 2 Trade|Trading Post'),
        ]
        gs.command_queue.append(commands)

    def scrap_action(self, gs: GameState):
        gs.add_combat(3)


CORE_SET_CARD_COLORS = {
    # BASIC
    'Scout': [CardColor.NONE],
    'Viper': [CardColor.NONE],
    'Explorer': [CardColor.NONE],
    # BLOB
    'Battle Blob': [CardColor.GREEN],
    'Battle Pod': [CardColor.GREEN],
    'Blob Carrier': [CardColor.GREEN],
    'Blob Destroyer': [CardColor.GREEN],
    'Blob Fighter': [CardColor.GREEN],
    'Blob Wheel': [CardColor.GREEN],
    'Blob World': [CardColor.GREEN],
    'Mothership': [CardColor.GREEN],
    'Ram': [CardColor.GREEN],
    'The Hive': [CardColor.GREEN],
    'Trade Pod': [CardColor.GREEN],
    # MACHINE CULT
    'Battle Mech': [CardColor.RED],
    'Battle Station': [CardColor.RED],
    'Brain World': [CardColor.RED],
    'Junkyard': [CardColor.RED],
    'Machine Base': [CardColor.RED],
    'Mech World': [CardColor.RED, CardColor.GREEN, CardColor.BLUE, CardColor.YELLOW],
    'Missile Bot': [CardColor.RED],
    'Missile Mech': [CardColor.RED],
    'Patrol Mech': [CardColor.RED],
    'Stealth Needle': [CardColor.RED],
    'Supply Bot': [CardColor.RED],
    'Trade Bot': [CardColor.RED],
    # STAR EMPIRE
    'Battlecruiser': [CardColor.YELLOW],
    'Corvette': [CardColor.YELLOW],
    'Dreadnaught': [CardColor.YELLOW],
    'Fleet HQ': [CardColor.YELLOW],
    'Imperial Fighter': [CardColor.YELLOW],
    'Imperial Frigate': [CardColor.YELLOW],
    'Recycling Station': [CardColor.YELLOW],
    'Royal Redoubt': [CardColor.YELLOW],
    'Space Station': [CardColor.YELLOW],
    'Survey Ship': [CardColor.YELLOW],
    'War World': [CardColor.YELLOW],
    # TRADE FEDERATION
    'Barter World': [CardColor.BLUE],
    'Central Office': [CardColor.BLUE],
    'Command Ship': [CardColor.BLUE],
    'Cutter': [CardColor.BLUE],
    'Defense Center': [CardColor.BLUE],
    'Embassy Yacht': [CardColor.BLUE],
    'Federation Shuttle': [CardColor.BLUE],
    'Flagship': [CardColor.BLUE],
    'Freighter': [CardColor.BLUE],
    'Port of Call': [CardColor.BLUE],
    'Trade Escort': [CardColor.BLUE],
    'Trading Post': [CardColor.BLUE],
}