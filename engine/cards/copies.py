from core_set import *

CORE_SET_COPIES = {
    # BLOB
    BattleBlob: 1,
    BattlePod: 2,
    BlobCarrier: 1,
    BlobDestroyer: 2,
    BlobFighter: 3,
    BlobWheel: 3,
    BlobWorld: 1,
    Mothership: 1,
    Ram: 2,
    TheHive: 1,
    TradePod: 3,
    # MACHINE CULT
    BattleMech: 1,
    BattleStation: 2,
    BrainWorld: 1,
    Junkyard: 1,
    MachineBase: 1,
    MechWorld: 1,
    MissileBot: 3,
    MissileMech: 1,
    PatrolMech: 2,
    StealthNeedle: 1,
    SupplyBot: 3,
    TradeBot: 3,
    # STAR EMPIRE
    Battlecruiser: 1,
    Corvette: 2,
    Dreadnaught: 1,
    FleetHQ: 1,
    ImperialFighter: 3,
    ImperialFrigate: 3,
    RecyclingStation: 2,
    RoyalRedoubt: 1,
    SpaceStation: 2,
    SurveyShip: 3,
    WarWorld: 1,
    # TRADE FEDERATION
    BarterWorld: 2,
    CentralOffice: 1,
    CommandShip: 1,
    Cutter: 3,
    DefenseCenter: 1,
    EmbassyYacht: 2,
    FederationShuttle: 3,
    Flagship: 1,
    Freighter: 2,
    PortofCall: 1,
    TradeEscort: 1,
    TradingPost: 2,
}

STARTER_DECK_CARDS = {
    Scout: 8,
    Viper: 2,
}