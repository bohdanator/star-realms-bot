from game.constants import CardColor, CardType, GameActionError
from typing import Any

factions = {CardColor.GREEN:'Blob', CardColor.RED:'Machine Cult', CardColor.BLUE:'Trade Federation', CardColor.YELLOW:'Star Empire'}

class Card:
    def __init__(self, name: str, colors: list[CardColor], cost: int, scrapable: bool =False, alliable: bool =True) -> None:
        self.colors: list[CardColor] = colors
        self.cost: int = cost
        self.scrapable: bool = scrapable
        self.alliable: bool = alliable
        self.name: str = name
        self.activated_primary: bool = False
        self.activated_secondary: bool = False
        self.activated_scrap: bool = False
        self.type: CardType = CardType.UNDEFINED

    def copy(self) -> Any:
        c = Card(self.name, self.colors, self.cost, self.scrapable, self.alliable)
        c.activated_primary = self.activated_primary
        c.activated_scrap = self.activated_scrap
        c.activated_secondary = self.activated_secondary
        c.type = self.type
        return c

    def __str__(self) -> str:
        return self.name

    def primary(self, gs) -> None:
        if self.activated_primary:
            raise GameActionError(f"{self.name}: Primary action already activated")
        self.activated_primary = True
        self.primary_action(gs)

    def secondary(self, gs) -> None:
        if self.activated_secondary:
            raise GameActionError(f"{self.name}: Secondary action already activated")
        self.activated_secondary = True
        self.secondary_action(gs)

    def scrap(self, gs) -> None:
        if self.activated_scrap: return
        self.activated_scrap = True
        self.scrap_action(gs)

    def primary_action(self, gs) -> None:
        pass

    def secondary_action(self, gs) -> None:
        pass

    def scrap_action(self, gs) -> None:
        pass

    def reset(self) -> None:
        self.activated_primary = False
        self.activated_secondary = False
