from cards.Card import Card
from game.constants import CardColor, CardType

class Base(Card):
    def __init__(
            self, name: str,
            colors: list[CardColor],
            cost: int,
            defence: int,
            outpost: bool,
            scrapable: bool =False,
            alliable: bool =True,
        ) -> None:
        super().__init__(name, colors, cost, scrapable, alliable)
        self.type = CardType.BASE
        self.defence = defence
        self.hp = defence
        self.outpost = outpost

    def reset(self) -> None:
        self.hp = self.defence
        super().reset()

    def copy(self):
        c = super().copy(self)
        c.defence = self.defence
        c.outpost = self.outpost
        return c