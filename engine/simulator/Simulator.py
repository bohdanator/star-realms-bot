from game.GameState import GameStateRepr
from typing import Callable
from game.Game import Game
from game.GameInitStrategy import GameInitStrategy
from game.CommandQueueStrategy import CommandQueueStrategy
from bots.Bot import Bot

class Simulator:
    def __init__(
        self,
        init_strat: GameInitStrategy,
        cq_strat: CommandQueueStrategy,
        name1: str,
        cls1: Bot,
        name2: str,
        cls2: Bot,
        max_choices: int =500) -> None:

        self.classes = {
            name1:cls1,
            name2:cls2,
        }

        self.game = Game(init_strat, cq_strat, [name1, name2])
        self.turns = 0
        self.max_choices = max_choices

    def step(self, vocal: bool =False) -> None:
        cls = self.classes[self.game.gs.player_on_turn.name]
        a = cls.process(GameStateRepr(self.game.gs))
        if vocal:
            print(a, self.game.command_queue[0][a].description)
        try:
            self.game.process_action(a)
        except Exception as e:
            print(e)
            print('CHOICE', a)
            print(self.game.command_queue)
            print([c.description for c in self.game.command_queue[-1]])
            raise e

    def run(self, vocal: bool =False, results: bool =True) -> None:
        while not self.game.game_over:
            self.step(vocal)
            self.turns += 1
            if self.turns > self.max_choices:
                break
            for p in self.game.gs.players:
                if p.hp <= 0 and not self.game.game_over:
                    print(f'PLAYER \'{p.name}\' HAS NEGATIVE HP')
                    self.results()
                    return
        if results: self.results()

    def results(self) -> None:
        print(self.game.gs.players[0].name)
        print('Combat:', self.game.gs.players[0].combat, ', Trade:', self.game.gs.players[0].trade, ', HP:', self.game.gs.players[0].hp)
        print('### HAND ###')
        for c in self.game.gs.players[0].hand:
            print(c.name, end=' | ')
        print('\n### BATTLEFIELD ###')
        for c in self.game.gs.players[0].battlefield:
            print(c.name, end=' | ')
        print('\n### DISCARD ###')
        for c in self.game.gs.players[0].discard_pile:
            print(c.name, end=' | ')
        print('\n### TRADE ROW ###')
        for c in self.game.gs.shop:
            print(c.name, end=' | ')
        print()
        print(self.game.gs.players[1].name)
        print('Combat:', self.game.gs.players[1].combat, ', Trade:', self.game.gs.players[1].trade, ', HP:', self.game.gs.players[1].hp)
        print('### HAND ###')
        for c in self.game.gs.players[1].hand:
            print(c.name, end=' | ')
        print('\n### BATTLEFIELD ###')
        for c in self.game.gs.players[1].battlefield:
            print(c.name, end=' | ')
        print('\n### DISCARD ###')
        for c in self.game.gs.players[1].discard_pile:
            print(c.name, end=' | ')
        print('\n### TRADE ROW ###')
        for c in self.game.gs.shop:
            print(c.name, end=' | ')
        print()
        print('GAME OVER?', self.game.game_over)
