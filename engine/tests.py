from typing import Callable
from game.Command import Command

class TestGS:
    command_queue = []
    queue = []

dn = lambda *args: None

def add_serial_test(gs: list, f: Callable, g: Callable =dn) -> None:
    """
    Takes a list of functions that return list[Command]
    and adds them to GameState gs so that when one choice
    is made, next choice is added.
    """
    return lambda ngs, ndf=dn: ngs.command_queue.append(f(ngs, g))

def add1(gs, df=dn):
    def wrapper(gs, *args, **kwargs):
        gs.queue.append(1)
        df(gs)
    return [Command(wrapper, 'add1')]

def add2(gs, df=dn):
    def wrapper(gs, *args, **kwargs):
        gs.queue.append(2)
        df(gs)
    return [Command(wrapper, 'add2')]

def add3(gs, df=dn):
    def wrapper(gs, *args, **kwargs):
        gs.queue.append(3)
        df(gs)
    return [Command(wrapper, 'add3')]

tgs = TestGS()
add_serial_test(tgs, add1, add_serial_test(tgs, add2, add_serial_test(tgs, add3)))(tgs)
print(tgs.queue, tgs.command_queue)
i = 0
while not len(tgs.command_queue) == 0 and i < 10:
    i += 1
    print('===================================')
    print(tgs.queue)
    for c in tgs.command_queue[0]: print(c.description)
    cmds = tgs.command_queue.pop(0)
    cmds[0].execute(tgs)
    print(tgs.queue)
    print('===================================')
