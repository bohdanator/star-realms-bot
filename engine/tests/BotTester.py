from random import Random
from game.constants import TurnState
from simulator.Simulator import Simulator
from bots.ZeroBot import ZeroBot
from bots.RandomBot import RandomBot
from bots.BasicSmartBot import BasicSmartBot
from cards.TwoPlayerBasicInit import TwoPlayerBasicInit
from cards.TwoPlayerCQ import TwoPlayerCQ
import sys

class BotTester:
    def __init__(self, n_tests, max_choices, init, cq, *args):
        self.init = init
        self.max_choices = max_choices
        self.cq = cq
        self.args = args
        self.n = n_tests

    def run(self, vocal=True):
        stats = {}
        for t in range(self.n):
            sim = Simulator(
                self.init,
                self.cq,
                self.args[0],
                self.args[1](),
                self.args[2],
                self.args[3](),
                max_choices=self.max_choices
            )
            sim.run(results=False)
            for i, p in enumerate(sim.game.gs.players):
                if not p.turn_state == TurnState.DEAD:
                    if vocal: print(t, '|', p.name, 'WON')
                    if p.name in stats.keys():
                        stats[p.name] += 1
                    else:
                        stats[p.name] = 1
        print(stats)

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print(
"""
Usage:
First argument: number of tests
Second and Third arguments: The bots you want to test.
    Currently available bots: RandomBot, ZeroBot, SmartBot
"""
        )
        quit()
    botcls = {
        'RandomBot':RandomBot,
        'ZeroBot':ZeroBot,
        'SmartBot':BasicSmartBot,
    }
    try:
        bt = BotTester(
            int(sys.argv[1]),
            2000,
            TwoPlayerBasicInit(),
            TwoPlayerCQ(),
            sys.argv[2],
            botcls[sys.argv[2]],
            sys.argv[3],
            botcls[sys.argv[3]],
        )
        bt.run(vocal=False)
    except Exception as e:
        print(e)
        print(
"""
Usage:
First argument: number of tests
Second and Third arguments: The bots you want to test.
    Currently available bots: RandomBot, ZeroBot, SmartBot
"""
        )