from cards.Card import Card
from game.Player import Player, PlayerRepr
from game.Command import Command
from game.constants import CardColor, game_action, GameActionError, TurnState, CardType
from typing import Generator, Callable, Any
from itertools import chain

class GameState:
    def __init__(self):
        self.players: list[Player] = []
        self.first_player = 0
        self.deck: list[Card] = []
        self.trade_row: list[Card] = []
        self.permanent_trade_row_classes: list[Any] = []
        self.trade_row_size: int = 0
        self.turn_number: int = 0
        self.play_triggers: list[Callable] = []
        self.next_on_topdeck: bool = False
        self.game_over = False

    @property
    def player_on_turn(self) -> Player:
        return self.players[(self.first_player + self.turn_number) % len(self.players)]

    @property
    def command_queue(self) -> list[list[Command]]:
        return self.player_on_turn.command_queue

    @property
    def hand(self) -> list[Card]:
        return self.player_on_turn.hand

    @property
    def discard_pile(self) -> list[Card]:
        return self.player_on_turn.discard_pile

    @property
    def battlefield(self) -> list[Card]:
        return list(chain([p.battlefield for p in self.players]))

    @property
    def cards_scrapped(self) -> int:
        return self.player_on_turn.cards_scrapped

    @property
    def colors_played(self) -> dict[CardColor, int]:
        return self.player_on_turn.colors_played

    @property
    def shop(self) -> list[Card]:
        return self.trade_row + [c() for c in self.permanent_trade_row_classes]

    def end_turn(self) -> None:
        self.play_triggers = []
        self.player_on_turn.end_turn()
        self.turn_number += 1
        counter = 0
        while self.player_on_turn.turn_state == TurnState.DEAD:
            self.turn_number += 1
            counter += 1
            if counter == len(self.players):
                self.game_over = True
                return
        self.player_on_turn.turn_state = TurnState.ACTIVE
        self.player_on_turn.start_turn()

    def check_dead(self) -> None:
        alive = 0
        for p in self.players:
            if p.turn_state == TurnState.DEAD:
                continue
            if p.hp <= 0:
                p.die()
                continue
            alive += 1
        if alive <= 1:
            self.game_over = True

    def add_trade(self, n) -> None: self.player_on_turn.trade += n

    def add_combat(self, n) -> None: self.player_on_turn.combat += n

    def add_authority(self, n) -> None: self.player_on_turn.hp += n

    def draw(self, n=1) -> None:
        for _ in range(n):
            self.player_on_turn.draw()

    @game_action('trade index out of range')
    def buy(self, ind: int) -> None:
        c = self.acquire_card(ind)
        if self.next_on_topdeck:
            self.player_on_turn.deck.append(c)
            self.next_on_topdeck = False
        else:
            self.player_on_turn.discard_pile.append(c)
        self.refill_trade_row()

    @game_action('hand index out of range')
    def play(self, ind: int) -> None:
        c = self.hand.pop(ind)
        self.player_on_turn.battlefield.append(c)
        for col in c.colors:
            self.player_on_turn.colors_played[col] += 1
        c.primary(self)
        for t in self.play_triggers:
            t(self, c)

    @game_action('battlefield index out of range')
    def scrap_by_ability(self, ind: int) -> None:
        c = self.player_on_turn.battlefield.pop(ind)
        if not c.scrapable:
            self.player_on_turn.battlefield.append(c)
            raise GameActionError('Card not scrapable')
        c.scrap(self)

    @game_action('hand index out of range')
    def discard(self, ind: int) -> None:
        self.discard_pile.append(self.hand.pop(ind))

    def refill_trade_row(self) -> None:
        while len(self.trade_row) < self.trade_row_size and len(self.deck) > 0:
            self.trade_row.append(self.deck.pop())
        if len(self.trade_row) == 0 and len(self.deck) == 0:
            self.game_over = True

    def get_card_targets(self) -> Generator[Card, None, None]:
        for p in self.players:
            for c in p.battlefield:
                if c.type == CardType.BASE:
                    yield c

    @game_action('trade row index out of range')
    def acquire_card(self, ind: int, free: bool =False) -> Card:
        """
        Action that returns a specific card from the trade row.
        ARGUMENTS: INDEX, FREE
        """
        if not free and self.player_on_turn.trade < self.shop[ind].cost:
            raise GameActionError("not enough trade")
        if ind < len(self.trade_row):
            c = self.trade_row.pop(ind)
        else:
            c = self.shop.pop(ind)
        if not free:
            self.player_on_turn.trade -= c.cost
        self.refill_trade_row()
        return c

    @game_action('trade row index out of range')
    def scrap_from_trade_row(self, ind: int) -> None:
        """
        Action that scraps a card from the trade row.
        ARGUMENTS: INDEX
        """
        del self.trade_row[ind]


    @game_action('hand index out of range')
    def scrap_from_hand(self, ind: int) -> None:
        """
        Action that scraps a card from hand
        ARGUMENTS: INDEX
        """
        del self.player_on_turn.hand[ind]
        self.player_on_turn.cards_scrapped += 1


    @game_action('discard index out of range')
    def scrap_from_discard(self, ind: int) -> None:
        """
        Action that scraps card from discard
        ARGUMENTS: INDEX
        """
        del self.player_on_turn.discard_pile[ind]
        self.player_on_turn.cards_scrapped += 1

    def put_on_topdeck(self, card: Card) -> None:
        """
        Action that puts a card on topdeck
        ARGUMENTS: CARD
        """
        self.player_on_turn.deck.append(card)

    @game_action('trade row index out of range')
    def free_card_on_topdeck(self, ind: int) -> None:
        """
        Action that acquires a card for free and puts on topdeck
        ARGUMENTS: INDEX
        """
        self.put_on_topdeck(self.acquire_card(ind, free=True))

    @game_action('discard empty')
    def last_bought_on_topdeck(self) -> None:
        self.put_on_topdeck(self.player_on_turn.discard.pop(-1))

    @game_action('player or battlefield index out of range')
    def destroy_card(self, p: int, ind: int) -> None:
        """
        Action that destroys target card on the battlefield
        ARGUMENTS: PLAYER, INDEX
        """
        self.players[p].destroy(ind)

    @game_action('player index out of range')
    def damage_player(self, p: int) -> None:
        self.players[p].hp -= self.player_on_turn.combat
        self.player_on_turn.combat = 0
        if self.players[p].hp <= 0:
            self.player_on_turn.combat -= self.players[p].hp
            self.players[p].die()
            self.check_dead()

class GameStateRepr:
    def __init__(self, gs: GameState, requester: int =-1) -> None:
        self.players: list[PlayerRepr] = [PlayerRepr(p) for p in gs.players]
        self.first_player = gs.first_player
        self.trade_row: list[str] = [card.name for card in gs.trade_row]
        self.permanent_trade_row_classes: list[Any] = [cl for cl in gs.permanent_trade_row_classes]
        self.turn_number: int = gs.turn_number
        self.play_triggers: list[Callable] = [t for t in gs.play_triggers]
        self.next_on_topdeck: bool = gs.next_on_topdeck
        self.game_over = gs.game_over
        self.command_queue: list[list[str]] = [[c.description for c in commands] for commands in gs.command_queue]
        if requester > -1: self.players[requester] = PlayerRepr(gs.players[requester], True)
