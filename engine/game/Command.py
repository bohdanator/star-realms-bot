from typing import Callable, Any

class Command:
    def __init__(self, action: Callable, description: str, *args, **kwargs) -> None:
        self.description = description
        self.action = action
        self.args = args
        self.kwargs = kwargs

    def execute(self, gs: Any) -> None:
        self.action(gs, *self.args, **self.kwargs)