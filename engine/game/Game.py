from game.GameState import GameState
from game_init.GameInitStrategy import GameInitStrategy
from command_queue.CommandQueueStrategy import CommandQueueStrategy
from game.constants import GameActionError, TurnState, game_action

class Game:
    def __init__(
        self,
        init_strategy: GameInitStrategy,
        cq_strategy: CommandQueueStrategy,
        players: list[str],
        ) -> None:
        self.gs = GameState()
        init_strategy.initialize(self.gs, players)
        self._cq_strategy = cq_strategy
        self._cq_strategy.create_cq(self.gs)

    @property
    def command_queue(self):
        return self.gs.command_queue

    @property
    def game_over(self):
        return self.gs.game_over

    @game_action('command index out of range')
    def process_action(self, ind: int) -> None:
        if self.game_over:
            raise GameActionError('trying to execute a command when the game is over')
        command = self.gs.command_queue[0][ind]
        self.gs.command_queue.pop(0)
        command.execute(self.gs)
        if self.gs.player_on_turn.turn_state == TurnState.ENDING:
            self.gs.end_turn()
            if self.game_over:
                return
        while len(self.command_queue) > 0 and len(self.command_queue[0]) == 0:
            self.command_queue.pop(0)
        if len(self.gs.command_queue) == 0:
            self._cq_strategy.create_cq(self.gs)
