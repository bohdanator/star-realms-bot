from typing import Callable
from game.Command import Command
from game.constants import TurnState, GameActionError, CardType, CardColor, game_action
from cards.Card import Card
import random

class Player:
    def __init__(self, name: str) -> None:
        self.name: str = name
        self.deck: list[Card] = []
        self.hp: int = 0
        self.hand: list[Card] = []
        self.discard_pile: list[Card] = []
        self.battlefield: list[Card] = []
        self.trade: int = 0
        self.combat: int = 0
        self.colors_played: dict[CardColor, int] = {color:0 for color in CardColor}
        self.cards_scrapped: int = 0
        self.cards_to_discard = 0
        self.turn_state: TurnState = TurnState.WAITING
        self.command_queue: list[list[Command]] = []

    def __eq__(self, o: object) -> bool:
        return o.name == self.name

    def draw(self) -> None:
        if len(self.deck) == 0: self.reset_deck()
        if len(self.deck) > 0: self.hand.append(self.deck.pop())

    def allied(self, color: CardColor) -> bool:
        n = 0
        for c in self.battlefield:
            if color in c.colors:
                n += 1
                if n >= 2:
                    return True
        return False

    def start_turn(self) -> None:
        for i in range(self.cards_to_discard):
            cmds = [
                Command(
                    lambda gs, x: gs.player_on_turn.discard(x),
                    f'DISCARD {j} ME|Discard {j}th card from your hand',
                    j,
                ) for j in range(len(self.hand) - i)
            ]
            self.command_queue.append(cmds)
        self.cards_to_discard = 0

    @game_action('played card index out of range')
    def play(self, ind: int) -> None:
        c = self.hand.pop(ind)
        for color in c.colors:
            self.colors_played[color] += 1
        self.battlefield.append(c)

    @game_action('discarded card out of range')
    def discard(self, ind: int =-1) -> None:
        self.discard_pile.append(self.hand.pop(ind))

    @game_action('destroyed card index our of range')
    def destroy(self, ind: int) -> None:
        if self.battlefield[ind].type != CardType.BASE:
            raise GameActionError('Card not destructible')
        self.discard_pile.append(self.battlefield.pop(ind))
        self.discard_pile[-1].reset()

    @game_action('scrapped card out of range')
    def scrap(self, ind: int) -> None:
        del self.battlefield[ind]

    def reset_deck(self) -> None:
        self.deck = self.discard_pile
        self.discard_pile = []
        random.shuffle(self.deck)

    def reset_battlefield(self) -> None:
        for c in self.battlefield:
            c.reset()

    def cleanup(self) -> None:
        ind = 0
        while ind < len(self.battlefield):
            card = self.battlefield[ind]
            card.reset()
            if card.type == CardType.SHIP:
                self.battlefield.pop(ind)
                self.discard_pile.append(card)
            else:
                ind += 1
        self.colors_played = {color:0 for color in CardColor}
        self.trade = 0
        self.combat = 0
        self.cards_scrapped = 0

    def end_turn(self) -> None:
        while len(self.hand) > 0:
            self.discard()
        self.cleanup()
        for _ in range(5):
            self.draw()
        self.turn_state = TurnState.WAITING

    def die(self) -> None:
        self.turn_state = TurnState.DEAD
        while len(self.battlefield) > 0:
            self.discard_pile.append(self.battlefield.pop())
        while len(self.hand) > 0:
            self.discard_pile.append(self.hand.pop())
        while len(self.deck) > 0:
            self.discard_pile.append(self.deck.pop())

class PlayerRepr:
    def __init__(self, player: Player, me: bool =False) -> None:
        self.name = player.name
        self.hp = player.hp
        self.deck = [None for _ in player.deck]
        self.discard_pile = [card.copy() for card in player.discard_pile]
        self.battlefield = [card.copy() for card in player.battlefield]
        self.cards_to_discard = player.cards_to_discard
        self.colors_played = {}
        for key, val in player.colors_played.items(): self.colors_played[key] = val
        self.cards_scrapped = player.cards_scrapped
        self.hand = [None for _ in player.hand]
        self.trade = player.trade
        self.combat = player.combat
        if me:
            self.hand = [card.copy() for card in player.hand]
