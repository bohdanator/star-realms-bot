from enum import Enum

class GameActionError(Exception):
    pass

class CardType(Enum):
    UNDEFINED = 1
    SHIP = 2
    BASE = 3

class CardColor(Enum):
    NONE = 1
    GREEN = 2
    RED = 3
    BLUE = 4
    YELLOW = 5

class TurnState(Enum):
    WAITING = 1
    ACTIVE = 2
    ENDING = 3
    DEAD = 4

def game_action(msg):
    """
    Decorator to replace IndexError with GameActionError with msg argument.
    """
    def decorator(f):
        def wrapper(*args, **kwargs):
            try:
                return f(*args, **kwargs)
            except IndexError:
                raise GameActionError(msg)
        return wrapper
    return decorator