from typing import Callable
from cards.Card import Card
from game.GameState import GameState
from game.Command import Command
from game.constants import game_action, CardType
from itertools import chain

# HELPER FUNCTIONS

dn = lambda *args: None

do_nothing_command = Command(dn, 'NOTHING|Do nothing from these options')

def do_nothing_queue(*args, **kwargs):
    return [do_nothing_command]

def create_trade_row_scrap_queue(gs: GameState, decorating_f: Callable =dn) -> list[Command]:
    def wrapper(gs, *args, **kwargs):
        gs.scrap_from_trade_row(*args, **kwargs)
        decorating_f(gs)
    commands = []
    for i, card in enumerate(gs.trade_row):
        commands.append(Command(
            wrapper,
            f'SCRAP {i} TRADE|Scrap {i}th card from trade row, {card.name}',
            i,
        ))
    return commands

def create_free_ship_on_topdeck_queue(gs: GameState, decorating_f: Callable =dn) -> list[Command]:
    def wrapper(gs, *args, **kwargs):
        gs.free_card_on_topdeck(*args, **kwargs)
        decorating_f(gs)
    commands = []
    for i, c in enumerate(gs.trade_row):
        if c.type == CardType.SHIP:
            commands.append(Command(
                wrapper,
                f'BUY {i} FREE TOPDECK|Acquire {i}th card, {c.name}, for free and put on topdeck|{c.name}',
                i,
            ))
    return commands

def create_destroy_base_queue(gs: GameState, decorating_f: Callable =dn) -> list[Command]:
    def wrapper(gs, *args, **kwargs):
        gs.destroy_card(*args, **kwargs)
        decorating_f(gs)
    commands = []
    for p, player in enumerate(gs.players):
        for c, card in enumerate(player.battlefield):
            if card.type == CardType.BASE:
                commands.append(Command(
                    wrapper,
                    f'DESTROY {p} {c}|Destroy base "{card.name}" of player "{player.name}"',
                    p,
                    c,
                ))
    return commands

def create_discard_scrap_queue(gs: GameState, decorating_f: Callable =dn) -> list[Command]:
    def wrapper(gs, *args, **kwargs):
        gs.scrap_from_discard(*args, **kwargs)
        decorating_f(gs)
    commands = []
    for c, card in enumerate(gs.player_on_turn.discard_pile):
        commands.append(Command(
            wrapper,
            f'SCRAP {c} DISCARD|Scrap card "{card.name}" from your discard',
            c,
        ))
    return commands

def create_hand_scrap_queue(gs: GameState, decorating_f: Callable =dn) -> list[Command]:
    def wrapper(gs, *args, **kwargs):
        gs.scrap_from_hand(*args, **kwargs)
        decorating_f(gs)
    commands = []
    for c, card in enumerate(gs.player_on_turn.hand):
        commands.append(Command(
            wrapper,
            f'SCRAP {c} HAND|Scrap card "{card.name}" from your hand',
            c,
        ))
    return commands

def create_discard_queue(gs: GameState, decorating_f: Callable =dn, pind=-1) -> list[Command]:
    def wrapper(gs, *args, **kwargs):
        gs.discard(*args, **kwargs)
        decorating_f(gs)
    commands = []
    player = (gs.player_on_turn if pind == -1 else gs.players[pind])
    for c, card in enumerate(player.hand):
        commands.append(Command(
            wrapper,
            f'DISCARD {c} ME|Discard card "{card.name}" from your hand',
            c,
        ))
    return commands

def create_player_discard_queue(gs: GameState, decorating_f: Callable =dn) -> list[Command]:
    def wrapper(gs, p, *args, **kwargs):
        gs.players[p].cards_to_discard += 1
        decorating_f(gs)
    commands = []
    for p, player in enumerate(gs.players):
        commands.append(Command(
            wrapper,
            f'DISCARD {p} OPPONENT|Player "{player.name}" will discard a card',
            p,
        ))
    return commands

def create_paralell(gs: GameState, *args: list[Callable]) -> list[Command]:
    """
    Takes a list of functions that return list[Command]
    and returns a function that creates the sum of these list[Command].
    """
    return lambda gs, df=dn: list(chain(*[f(gs, df) for f in args]))

def add_serial(gs: GameState, queue_f: Callable, decor_f: Callable =dn) -> None:
    """
    Takes a queue creating function and decorated function
    and chains them together.
    This way it is possible to chain multiple functions:
    f = add_serial(gs, add_commands, add_serial(gs, add_other_commands, ...)).
    f(gs) then adds created commands to the GameState
    After the resolution of one Command then there automatically are
    other commands in the command queue.
    """
    return lambda ngs, ndf=dn: ngs.command_queue.append(queue_f(ngs, decor_f))
