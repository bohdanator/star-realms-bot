from game.GameState import GameState
from typing import Any

class GameInitStrategy:
    def initialize(self, gs: GameState, players: list[str]) -> None:
        pass