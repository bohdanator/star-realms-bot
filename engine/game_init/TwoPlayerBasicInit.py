import random

from cards.copies import CORE_SET_COPIES, STARTER_DECK_CARDS
from cards.core_set import *
from game.constants import GameActionError, TurnState
from game.GameState import GameState
from game.Player import Player

from game_init.GameInitStrategy import GameInitStrategy


class TwoPlayerBasicInit(GameInitStrategy):

    def initialize(self, gs: GameState, players: list[str]) -> None:
        if len(players) > 2:
            raise GameActionError('Using two player init for more than 2 players')
        gs.trade_row_size = 5
        gs.permanent_trade_row_classes = [Explorer]
        for c, n in CORE_SET_COPIES.items():
            for _ in range(n):
                gs.deck.append(c())
        random.shuffle(gs.deck)
        for name in players:
            p = Player(name)
            p.hp = 50
            for c, n in STARTER_DECK_CARDS.items():
                for _ in range(n):
                    p.deck.append(c())
            random.shuffle(p.deck)
            gs.players.append(p)
        gs.refill_trade_row()
        gs.first_player = random.randint(0, 1)
        for _ in range(3):
            gs.players[gs.first_player].draw()
        for _ in range(5):
            gs.players[(gs.first_player + 1) % 2].draw()
