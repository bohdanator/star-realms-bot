from game.GameState import GameStateRepr

class Bot:

    def process(self, gs: GameStateRepr) -> int:
        pass

class ParsedCommand:
    def __init__(self, description: str) -> None:
        m = description.split('|')
        self.origin = m[2] if len(m) >= 3 else 'None'
        self.args = m[0].split()
        self.action = self.args.pop(0)