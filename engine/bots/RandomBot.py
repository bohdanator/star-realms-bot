from bots.Bot import Bot
from random import randint
from game.GameState import GameStateRepr

class RandomBot(Bot):

    def process(self, gs: GameStateRepr) -> int:
        return randint(0, len(gs.command_queue[0])-1)