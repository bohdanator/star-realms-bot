from game.constants import CardColor
from bots.Bot import Bot, ParsedCommand
from game.GameState import GameStateRepr
from cards.core_set import CORE_SET_CARD_COLORS as CORE_COLS
import random


CARD_BUY_POWERS = {
    # BASIC
    'Scout': [-100],
    'Viper': [-100],
    'Explorer': [-1],
    # BLOB
    'Battle Blob': [0, 8, 10],
    'Battle Pod': [0],
    'Blob Carrier': [8],
    'Blob Destroyer': [0, 6, 7],
    'Blob Fighter': [4, 7],
    'Blob Wheel': [5, 2, 0],
    'Blob World': [7],
    'Mothership': [10],
    'Ram': [3],
    'The Hive': [5],
    'Trade Pod': [7, 3],
    # MACHINE CULT
    'Battle Mech': [8, 7, 6],
    'Battle Station': [3, 0],
    'Brain World': [10],
    'Junkyard': [8, 8, 7],
    'Machine Base': [10],
    'Mech World': [8],
    'Missile Bot': [8, 6, 4],
    'Missile Mech': [6, 8],
    'Patrol Mech': [7],
    'Stealth Needle': [10],
    'Supply Bot': [7, 5, 2],
    'Trade Bot': [7, 5, 2],
    # STAR EMPIRE
    'Battlecruiser': [9],
    'Corvette': [8, 7, 6],
    'Dreadnaught': [10],
    'Fleet HQ': [9],
    'Imperial Fighter': [4, 5],
    'Imperial Frigate': [4, 6],
    'Recycling Station': [7, 6, 5],
    'Royal Redoubt': [7, 7, 6],
    'Space Station': [7, 7, 6],
    'Survey Ship': [8, 8, 5],
    'War World': [5, 7, 5],
    # TRADE FEDERATION
    'Barter World': [7, 6, 5],
    'Central Office': [8],
    'Command Ship': [10],
    'Cutter': [7, 5, 2],
    'Defense Center': [6, 4, 2],
    'Embassy Yacht': [7, 8],
    'Federation Shuttle': [7, 5, 3],
    'Flagship': [9],
    'Freighter': [8, 6, 4],
    'Port of Call': [8],
    'Trade Escort': [5, 6, 7],
    'Trading Post': [6, 6, 2],
}

def card_buy_value(gs: GameStateRepr, cname: str, synergies: int):
    return CARD_BUY_POWERS[cname][min((gs.turn_number // len(gs.players)) // 3, len(CARD_BUY_POWERS[cname])-1)]*3 + synergies

class BasicSmartBot(Bot):
    def __init__(self) -> None:
        self.colors = {}
        for color in CardColor:
            self.colors[color] = 0

    def process(self, gs: GameStateRepr) -> int:
        """
        The idea is to implement a bot which doesn't make
        'mistakes'. Random or dumb bots often end the turn
        without damaging the opponent or do not use all the
        cards in their hand and abilities on the battlefield.
        This bot aims to activate all possible abilities, then
        scrap scouts and vipers first, then
        maybe buy some cards (maybe prioritize color already bought)
        (maybe assign power level to the cards based on turn number),
        always destroy opponents bases and not waste combat.
        """
        me = gs.players[gs.turn_number % len(gs.players)]
        commands = [ParsedCommand(d) for d in gs.command_queue[0]]
        # play everything
        # play non-basic cards first
        for ind, pc in enumerate(commands):
            if pc.action in ['PLAY', 'PRIMARY', 'SECONDARY'] and pc.origin not in ['Scout', 'Viper']:
                return ind
        # play basic cards
        for ind, pc in enumerate(commands):
            if pc.action in ['PLAY', 'PRIMARY', 'SECONDARY']:
                return ind
        # TODO implement custom choosing for each card, choose random for now
        for ind, pc in enumerate(commands):
            if pc.action == 'CHOICE':
                return 0 #random.randint(0, len(gs.command_queue[0])-1)
        # if scrapping, prioritize scouts and vipers
        # TODO choose least likely to buy to scrap from trade row
        for ind, pc in enumerate(commands):
            if pc.action == 'SCRAP' and pc.args[1] == 'TRADE':
                return ind
        for ind, pc in enumerate(commands):
            if pc.action == 'SCRAP' and pc.args[1] == 'DISCARD' and pc.origin in ['Scout', 'Viper']:
                return ind
        for ind, pc in enumerate(commands):
            if pc.action == 'SCRAP' and pc.args[1] == 'HAND' and pc.origin in ['Scout', 'Viper']:
                return ind
        for ind, pc in enumerate(commands):
            if pc.action == 'DISCARD' and pc.origin in ['Scout', 'Viper']:
                return ind
        # preemptively dont scrap special cards
        # buy most suitable card
        buyable = []
        for ind, pc in enumerate(commands):
            if pc.action == 'BUY':
                buyable.append((ind, pc))
        if len(buyable) > 0:
            vals = []
            for ind, pc in buyable:
                vals.append((
                    card_buy_value(gs, pc.origin, sum([self.colors[c] for c in CORE_COLS[pc.origin]])),
                    ind,
                    pc,
                ))
            vals.sort(key=lambda x: x[0], reverse=True)
            # print(list(map(lambda x: (x[0], x[1]), vals)))
            if vals[0][0] >= 0:
                for c in CORE_COLS[vals[0][2].origin]:
                    self.colors[c] += 1
                return vals[0][1]
        # prioritize destroying bases
        bases = []
        for ind, pc in enumerate(commands):
            if pc.action == 'COMBAT' and pc.args[0] == 'BASE':
                bases.append((ind, pc))
        if len(bases) > 0:
            # TODO choose most powerful base based on its power or more weaker bases not to waste combat
            return bases[0][0]
        # damage a player if possible
        for ind, pc in enumerate(commands):
            if pc.action == 'COMBAT' and pc.args[0] == 'PLAYER':
                return ind
        # end if nothing else is worth it
        for ind, pc in enumerate(commands):
            if pc.action == 'END':
                return ind
        # print('NOT PERFECT')
        # if dont know what to do, choose first to prevent errors
        return 0
