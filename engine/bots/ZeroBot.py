from bots.Bot import Bot
from game.GameState import GameStateRepr

class ZeroBot(Bot):

    def process(self, gs: GameStateRepr) -> int:
        return 0