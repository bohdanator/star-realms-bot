from game.Command import Command
from game.constants import CardType, TurnState
from game.GameState import GameState

from command_queue.CommandQueueStrategy import CommandQueueStrategy


class TwoPlayerCQ(CommandQueueStrategy):

    def _create_turn_cq(self, gs: GameState) -> list[Command]:
        pot = gs.player_on_turn
        queue = []
        # PLAY FROM HAND
        for i, c in enumerate(pot.hand):
            queue.append(Command(
                lambda gs, i: gs.play(i),
                f'PLAY {i}|Play card \'{c.name}\' and activate its primary ability|{c.name}',
                i,
            ))
        for i, c in enumerate(pot.battlefield):
            # LEFTOVER BASES
            if c.type == CardType.BASE and not c.activated_primary:
                queue.append(Command(
                    lambda gs, i: gs.player_on_turn.battlefield[i].primary(gs),
                    f'PRIMARY {i}|Activate primary ability of base \'{c.name}\'|{c.name}',
                    i,
                ))
        for i, c in enumerate(pot.battlefield):
            # SECONDARY ABILITIES
            allied = [pot.allied(col) for col in c.colors]
            if sum(allied) == 0:
                continue
            if c.alliable and not c.activated_secondary:
                queue.append(Command(
                    lambda gs, i: gs.player_on_turn.battlefield[i].secondary(gs),
                    f'SECONDARY {i}|Activate allied ability of card \'{c.name}\'|{c.name}',
                    i,
                ))
        for i, c in enumerate(pot.battlefield):
            # SCRAP ABILITIES
            if c.scrapable:
                queue.append(Command(
                    lambda gs, i: gs.scrap_by_ability(i),
                    f'TERNARY {i}|Scrap card \'{c.name}\' using its scrap ability|{c.name}',
                    i,
                ))
        # BUY STUFF
        for i, c in enumerate(gs.shop):
            if c.cost <= pot.trade:
                queue.append(Command(
                    lambda gs, i: gs.buy(i),
                    f'BUY {i} TRADE|Buy card \'{c.name}\' for {c.cost} Trade|{c.name}',
                    i,
                ))
        return queue

    def _create_combat_cq(self, gs: GameState) -> list[Command]:
        pot = gs.player_on_turn
        opp = gs.players[(gs.turn_number + 1) % len(gs.players)]
        queue = []
        outpost = False

        def destroy(gs, p, i, c):
            gs.destroy_card(p, i)
            gs.player_on_turn.combat -= c

        # ADD OUTPOSTS
        for i, base in enumerate(opp.battlefield):
            if base.type != CardType.BASE:
                continue
            if not base.outpost:
                continue
            outpost = outpost or base.outpost
            if base.hp > pot.combat:
                continue
            queue.append(Command(
                destroy,
                f'COMBAT BASE {(gs.turn_number + 1) % 2} {i}|Destroy base \'{base.name}\' with {base.hp} combat|{base.name}',
                (gs.turn_number + 1) % 2,
                i,
                base.hp,
            ))
        # ADD BASES AND PLAYER
        if not outpost and pot.combat > 0:
            for i, base in enumerate(opp.battlefield):
                if base.type != CardType.BASE:
                    continue
                if base.hp > pot.combat:
                    continue
                queue.append(Command(
                    destroy,
                    f'COMBAT BASE {(gs.turn_number + 1) % 2} {i}|Destroy base \'{base.name}\' with {base.hp} combat|{base.name}',
                    (gs.turn_number + 1) % 2,
                    i,
                    base.hp,
                ))
            queue.append(Command(
                lambda gs: gs.damage_player((gs.turn_number + 1) % 2),
                f'COMBAT PLAYER {(gs.turn_number + 1) % 2}|Deal the rest of your combat to your opponent',
            ))
        return queue

    def create_cq(self, gs: GameState) -> None:
        if len(gs.command_queue) > 0:
            return

        def ending(gs):
            gs.player_on_turn.turn_state = TurnState.ENDING

        end = Command(
            ending,
            'END|End your turn',
        )
        gs.command_queue.append(self._create_turn_cq(gs) + self._create_combat_cq(gs) + [end])
