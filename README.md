# Star Realms

## Implementácia hry
Hra je implementovaná v priečinku `game`. Stav hry sa udržuje v triede `GameState`, ktorú spravuje trieda `Game`. Celá partia Star Realms sa dá popísať postupnosťou týchto stavov a zmenu stavu vykonáva vždy iba jeden hráč. Preto sa táto zmena stavu vykonáva pomocou poľa herných možností `command_queue`, z ktorých si hráč vždy vyberie jednu. Tá sa potom aplikuje na daný stav a vytvorí sa nová sada príkazov.

## Implementácia botov

Toto umožňuje ľahko implementovať triviálnych botov. Program schopný hrať Star Realms *(bot)* je jedna trieda implementujúca metódu `process(gs: GameStateRepr) -> int`, ktorá dostane ako argument sprostredkovaný stav hry a vráti poradové číslo príkazu, ktorý program zvolil.

### Náhodný bot

Implementovaný v triede `RandomBot`. Zoberie pole príkazov a vyberie náhodný z nich. Teoreticky by mal byť veľmi neefektívny, lebo môže ukončiť ťah predtým, ako udelí poškodenie súperovi.

### Zero bot

Implementovaný v `ZeroBot`, tento bot vždy vyberie prvý príkaz. Funguje vďaka konštrukcii poľa príkazov. Do poľa príkazov sa pridávajú v poradí

1. Príkazy typu **Zahraj kartu z ruky**
2. Príkazy typu **Aktivuj primárnu abilitu vyloženej karty**
3. Príkazy typu **Aktivuj sekundárnu abilitu vyloženej karty**
4. Príkazy typu **Aktivuj terciárnu abilitu vyloženej karty**, ktorou sa karta vymaže z hry
5. Príkazy typu **Kúp novú kartu z obchodu**
6. Príkazy typu **Znič súperovi niektorú Základňu**
7. Príkazy typu **Udeľ zvyšok poškodenia súperovi**
8. Príkaz **Ukonči ťah**

Keďže pole príkazov je vždy neprázdne a keď už hráč vyčerpá všetky možnosti, obsahuje aspoň možnosť **Ukonči ťah**, tento bot je schopný korektne hrať. Navyše vďaka poradiu príkazov tento bot najprv zahrá a aktivuje všetky možné veci a až potom kupuje a udeľuje poškodenie. Teda má teoreticky možnosť kúpiť najdrahšie / najmocnejšie karty a nikdy nezahodí poškodenie. Teoreticky by mal byť celkom schopný.

### Bot so stratégiou

Implementovaný v triede `BasicSmartBot`. Testovanie ukázalo, že veľa hier sa rozhodne primárne tým, ktoré karty daný bot nakúpi. Cieľ tohto bota je 'nerobiť chyby' a zároveň mať stratégiu na kupovanie kariet.

Nerobenie chýb sa rieši podobným prístupom ako pri `ZeroBot`, teda najprv sa pokúsi zahrať všetky možné akcie kariet a až potom nakupuje / udeľuje poškodenie. To mu dáva najviac možností kúpenia silných kariet a ničenia silných základní.

Hra niekedy dáva botom možnosť úplne vymazať niektorú kartu z hry (*scrap*) buď jej vlastnou abilitou alebo niektorou inou kartou. Tento bot sa vždy pokúša kúpené karty nevymazávať, iba karty, s ktorými začína (`Scout`, `Viper`). Tu je ešte priestor na zlepšenie, lebo niektoré karty majú cenu práve v tom, že keď ich už nie je treba, sú schopné sa vymazať.

Bot má stratégiu na kupovanie kariet. Pre každú kartu som vyrobil pole čísel, z ktorých `i`-te vyjadruje silu danej karty v ťahoch `4i` až `4i+3` alebo do konca hry podľa mojej vlastnej skúsenosti. Toto by sa dalo postupne zlepšovať testovaním a hraním rôznych čísel proti sebe.

## Testovanie

Na testovanie som vytvoril triedu `BotTester` v priečinku `tests`. Otestovať sa momentálne dajú spomínaní traja boti aj s počtom testov a program potom vypíše, ktorý bot vyhral koľko hier. Hra má obmedzený počet výberov akcii, aby sa nehralo donekonečna, takže keď hra neskončí, remíza sa ráta ako výhra pre oboch. Použitie triedy `BotTester` sa vypíše príkazom

    python BotTester.py

ktorým sa zároveň aj testuje. Prvý argument je počet testov, druhý a tretí su boti, ktorí majú proti sebe hrať.

    $ python tests/BotTester.py

    Usage:
    First argument: number of tests
    Second and Third arguments: The bots you want to test.
        Currently available bots: RandomBot, ZeroBot, SmartBot

    $ python tests/BotTester.py 1000 SmartBot ZeroBot
    {'SmartBot': 581, 'ZeroBot': 428}